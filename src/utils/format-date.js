import { format } from 'date-fns/esm';

export default function formatDate(date) {

    // ГГГГММДДччммсс
    return format(date, 'yyyyMMddhhmmss')
    return '';
};
