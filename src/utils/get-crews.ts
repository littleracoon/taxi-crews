import Crews from './crews.json';

export interface CrewRequest {
    addresses: [
        {
            address: string,
            lat?: number,
            lon?: number
        }
    ]
    sourceTime: string
}

export default function getCrews(request: CrewRequest) {

    // crews fetching

    return new Promise((resolve) => resolve(Crews));
}
