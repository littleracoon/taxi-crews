import Order from './created-order.json';

export interface OrderRequest {
    addresses: [
        {
            address: string,
            lat?: number,
            lon?: number
        }
    ]
    sourceTime: string,
    crewId?: number
}

export default function createOrder(order: OrderRequest) {

    // order sending

    return new Promise((resolve) => resolve(Order));
}
