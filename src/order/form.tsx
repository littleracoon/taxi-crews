import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import createOrder, { OrderRequest } from '../utils/create-order';
import { connect } from 'react-redux';
import formatDate from '../utils/format-date';


function OrderForm({ state }: any) {

	const [orderId, setOrderId] = useState(undefined);

	return <Formik
		initialValues={{
			addresses: state.addresses,
			crewId: state.crew_id,
			sourceTime: formatDate(new Date()),
		}}
		onSubmit={(values: OrderRequest, actions: any) => {
			console.log(values);

			createOrder(values)
				.then((response: any) => {

					if (response.code === 0) {
						setOrderId(response.data.order_id)
					}

					console.log(response)
				})
		}}
		enableReinitialize={true}
		validateOnChange={true}
		validateOnMount={true}
		validateOnBlur={true}
	>
		{({ values, errors }: any) => {

			return (
				<Form id="createOrder">
					<div className="form-group row justify-content-center">
						<div className="col-lg-5 mb-4">
							<button className="mb-4 col-auto btn btn-primary d-block w-100" type="submit">Заказать</button>
						</div>

						{orderId && <div className="col-12 h3">Ваш заказ №{orderId} зарегистрирован!</div>}

					</div>

				</Form>
			)
		}}
	</Formik>
};


export default connect((state) => ({ state }))(OrderForm);