import React from 'react';
import Map from '../map';
import { connect } from 'react-redux';
import classNames from 'classnames';
import Search from '../search';
import CrewCurrent from '../card/crew-current';
import Collapse from 'react-bootstrap/Collapse';
import OrderForm from './form';
import { KEYS } from '../store';


function Order({ state, ...props }: any) {

	return <div className="container my-5">

		<Search />
		<Collapse in={Boolean(state.crews.length)}>
			<div>
				<div className="row mb-4">
					<div className="col-lg-3 text-lg-right mb-4">Подходящий экипаж</div>
					<div className="col-lg-7">
						<div className="row">
							<div className="col-lg-4">
								{state.crews.length ?
									<CrewCurrent
										crew={state.crews.find(
											(crew: any) => crew.crew_id === state.crewId) || state.crews[0]} /> :
									<div />}
							</div>
						</div>
					</div>
				</div>
			</div>
		</Collapse>
		<div className="row mb-5">
			<div className="col-lg-8">
				<Map />
			</div>
			<div className="col-lg-4">
				<Collapse in={Boolean(state.crews.length)}>
					<div className="list-group">
						{state.crews.map((crew: any, i: number) =>
							<button
								key={crew.crew_id}
								type="button"
								onClick={() => {
									props.dispatch({
										type: KEYS.CREW_ID, patch: crew.crew_id
									});
								}}
								className={classNames(
									'list-group-item',
									'list-group-item-action',
									'd-flex',
									'justify-content-between',
									{ 'active': crew.crew_id === state.crewId }
								)}>
								{crew.car_mark} {crew.car_model} <br />
								<span className={classNames('ml-2', {
									'text-muted': crew.crew_id !== state.crewId,
									'text-white': crew.crew_id === state.crewId
								})}>{crew.distance}М</span>
							</button>
						)}
					</div>
				</Collapse>
			</div>
		</div>
		<OrderForm />

	</div>

};


export default connect((state) => ({ state }))(Order);