export const requiredField = 'Обязательное поле';
export const notExactAddress = 'Неточный адрес, требуется уточнение';
export const incompleteAddress = 'Неполный адрес, требуется уточнение';
export const notFoundAddress = 'Адрес не найден';