import * as textErrors from '../text-notes/errors';

export const precision: any = {
    exact: {
        error: '',
        hint: ''
    },
    number: {
        error: textErrors.notExactAddress,
        hint: 'Уточните номер дома'
    },
    near: {
        error: textErrors.notExactAddress,
        hint: 'Уточните номер дома'
    },
    range: {
        error: textErrors.notExactAddress,
        hint: 'Уточните номер дома'
    },
    street: {
        error: textErrors.incompleteAddress,
        hint: 'Уточните номер дома'
    },
    other: {
        error: textErrors.notExactAddress,
        hint: 'Уточните адрес'
    },
    none: {
        error: 'Ошибка получения данных',
        hint: 'Попробуйте позже'
    },
    default: {
        error: textErrors.notExactAddress,
        hint: 'Уточните адрес'
    },
};

export default function checkAddress(request: string, fn: any): void {
    const ymaps = (window as any).ymaps;

    ymaps.geocode(request).then(function (res: { geoObjects: { get: (arg0: number) => any; }; }) {
        const obj = res.geoObjects.get(0);
        const precision = obj ? obj.properties.get('metaDataProperty.GeocoderMetaData.precision') : 'none';

        fn(precision, obj.geometry.getCoordinates());

    }, function (e: any) {
        console.log(e)
    })

};