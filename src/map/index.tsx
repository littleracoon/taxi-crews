import React from 'react';
import { connect } from 'react-redux';
import { KEYS } from '../store';
import checkAddress from './checkAddress';
import getCrews from '../utils/get-crews';
import formatDate from '../utils/format-date';

export interface Crew {
	crew_id: number,
	car_mark: string,
	car_model: string,
	car_color: string,
	car_number: string,
	driver_name: string,
	driver_phone: string,
	lat: number,
	lon: number,
	distance: number
};

export const ymaps = (window as any).ymaps;

function Map({ state, ...props }: any) {

	React.useEffect(() => {
		window.addEventListener('load', () => ymaps.ready(init));
	}, []);

	interface State {
		center: Array<number>;
		zoom: number;
	}

	const defaultState = {
		center: [56.852676, 53.206891],
		zoom: 12
	}

	function init() {
		let map = new ymaps.Map("map", defaultState);
		let placemark: any;
		// let suggestView = new ymaps.SuggestView('from', { provider: 'yandex#map' });
		const searchInput = (document as any).getElementById('from');
		const searchForm = (document as any).getElementById('search');

		searchForm.addEventListener('submit', () => {
			
			if (searchInput.value) {
				checkAddress(searchInput.value, (precision: string, coords: Array<number>) => {
					AddAddress(coords);
				});
			};
		});

		map.events.add('click', function (e: { get: (arg0: string) => any; }) {
			var coords = e.get('coords');

			AddAddress(coords);
		});

		function createPlacemark({ coords, preset = 'islands#redIcon', iconCaption = 'поиск...' }: any) {
			return new ymaps.Placemark(coords, {
				iconCaption
			}, {
				preset: preset,
				draggable: true
			});
		};

		function AddAddress(coords: any) {

			if (placemark) {
				placemark.geometry.setCoordinates(coords);
			}
			else {
				placemark = createPlacemark({ coords });
				map.geoObjects.add(placemark);

				placemark.events.add('dragend', function () {
					getAddress(placemark.geometry.getCoordinates(), (precision: string, address: string) => {

						setIcon(placemark, precision)
						addCrews(address, coords)
					});
				});
			}

			map.setCenter(coords)
			getAddress(coords, (precision: string, address: string) => {
				setIcon(placemark, precision);
				if (precision === 'exact') {
					addCrews(address, coords)
				}

			});
		};

		function setIcon(placemark: any, precision?: string, icon?: string) {
			if (precision) {
				if (precision === 'exact') {
					placemark.options.set('preset', 'islands#yellowIcon');
				} else {
					placemark.options.set('preset', 'islands#redIcon');
				}
			} else {
				placemark.options.set('preset', icon);
			}

		}

		function getAddress(coords: any, fn?: any) {
			placemark.properties.set('iconCaption', 'поиск...');
			ymaps.geocode(coords).then(function (res: { geoObjects: { get: (arg0: number) => any; }; }) {
				let firstGeoObject = res.geoObjects.get(0);

				const precision = firstGeoObject ? firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision') : 'none';

				fn(precision, firstGeoObject.getAddressLine());

				props.dispatch({
					type: KEYS.ADDRESSES_PATCH, patch: [{
						address: firstGeoObject.getAddressLine(),
						lat: coords[0],
						lon: coords[1]
					}]
				});

				placemark.properties
					.set({
						iconCaption: [
							firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
							firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
						].filter(Boolean).join(', '),
						balloonContent: firstGeoObject.getAddressLine()
					});
			});
		}

		function addCrews(address: string, coords: Array<number>) {
			getCrews({
				addresses: [
					{
						address,
						lat: coords[0],
						lon: coords[1]
					}
				],
				sourceTime: formatDate(new Date())
			})
				.then((response: any) => {

					if (response.code === 0) {
						props.dispatch({
							type: KEYS.CREWS_PATCH, patch: response.data.crews_info
						});
						props.dispatch({
							type: KEYS.CREW_ID, patch: response.data.crews_info[0].crew_id
						});
					}

					return new Promise((resolve) => resolve(response.data.crews_info))
				})
				.then((crews: any) => {

					crews.forEach((crew: Crew) => {

						let crewPlacemark = createPlacemark({
							coords: [crew.lat, crew.lon],
							preset: 'islands#greenIcon',
							iconCaption: `${crew.car_mark} ${crew.car_model}`
						});
						map.geoObjects.add(crewPlacemark);
					})
				})

		}

	};

	return <div className="map-container" id="map"></div>
};

export default connect((state) => ({ state }))(Map);