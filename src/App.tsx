import React from 'react';
import './App.scss';
import { Provider } from 'react-redux';
import initStore from './store';
import Order from './order';
import Navbar from 'react-bootstrap/Navbar';

function App() {
  return (
    <Provider store={initStore()}>
      <Navbar bg="primary" variant="dark" expand="lg">
        <Navbar.Brand href="#home">TAXI</Navbar.Brand>
      </Navbar>
      <div className="App">
        <Order />
      </div>
    </Provider>
  );
}

export default App;
