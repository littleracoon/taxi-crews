import * as redux from 'redux';

export const KEYS = {
  ADDRESSES_PATCH: 'ADDRESSES_PATCH',
  CREWS_PATCH: 'CREWS_PATCH',
  CREW_ID: 'CREW_ID',
};

export default function store() {
  const reducers = {
    addresses(
      state = [
        {
          address: '',
          lat: undefined,
          lon: undefined
        }
      ],
      action: { type: string; patch: any; },
    ) {
      if (action.type !== KEYS.ADDRESSES_PATCH) return state;

      return action.patch;
    },

    crews(
      state = [],
      action: { type: string; patch: any; },
    ) {
      if (action.type !== KEYS.CREWS_PATCH) return state;

      return action.patch;
    },

    crewId(
      state = '',
      action: { type: string; patch: any; },
    ) {
      if (action.type !== KEYS.CREW_ID) return state;

      return action.patch;
    },
  };

  return redux.createStore(redux.combineReducers(reducers));
}