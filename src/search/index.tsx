import React from 'react';
import { Formik, Field, Form, getIn } from 'formik';
import { requiredField } from '../text-notes/errors';
import getCrews from '../utils/get-crews';
import { OrderRequest } from '../utils/create-order';
import checkAddress, { precision as geoPrecision } from '../map/checkAddress';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { KEYS } from '../store';
import formatDate from '../utils/format-date';
import { resolve } from 'dns';


function Search({ state, ...props }: any) {

	return <Formik
		initialValues={{
			addresses: state.addresses,
			crewId: undefined,
			sourceTime: formatDate(new Date()),
		}}
		validate={(values: OrderRequest) => {

			return new Promise((resolve: any) => {

				const errors = {
					addresses: [
						{
							address: ''
						}
					]
				};

				if (!getIn(values, 'addresses[0].address')) {
					errors.addresses[0].address = requiredField;

					return errors;
				}

				checkAddress(getIn(values, 'addresses[0].address'), (precision: string, coords: Array<number>) => {

					const errorSet = precision in geoPrecision ?
						geoPrecision[precision] :
						geoPrecision.default;

					if (precision !== 'exact') {
						errors.addresses[0].address = Object.values(errorSet).join('. ');

						resolve(errors)
					}
				});
			})
				.then((errors: any) => errors)
		}}
		onSubmit={(values: OrderRequest, actions: any) => {}}
		enableReinitialize={true}
		validateOnChange={false}
		validateOnMount={true}
		validateOnBlur={true}
	>
		{({ values, errors, ...formProps }: any) => {
			const error = getIn(errors, 'addresses[0].address');

			return (
				<Form id="search">
					<div className="form-group row align-items-center">
						<label className={classNames(
							'col-lg-3',
							'text-lg-right',
							'mb-4',
							{ 'border-danger': error }
						)} htmlFor="from">Откуда</label>
						<div className="col-lg-7">
							<Field name="addresses[0].address">
								{({ field, form: { errors } }: any) => {

									const hasError = getIn(errors, field.name);

									return (
										<div className="position-relative mb-4">
											<input
												id="from"
												type="text"
												className="form-control mb-1"
												placeholder="введите место отправки ..."
												{...field} />
											{ hasError && (
												<div className="position-absolute text-danger text-small">{hasError}</div>
											)}
										</div>
									)
								}}
							</Field>
						</div>
						<div className="col-sm-auto">
							<button className="mb-4 btn btn-primary d-block w-100 mx-auto" type="submit">Поиск</button>
						</div>
					</div>

				</Form>
			)
		}}
	</Formik>
};


export default connect((state) => ({ state }))(Search);