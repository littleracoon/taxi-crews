import React from 'react';
import Card from 'react-bootstrap/Card'

export default function CrewCurrent({ crew: {
    car_mark,
    car_model,
    car_color,
    car_number
} }: any) {
    return <Card>
        <Card.Header>{car_mark} {car_model}</Card.Header>
        <Card.Body>
            <div>{car_color}</div>
            <div className="badge badge-secondary">{car_number}</div>
        </Card.Body>
    </Card>
}