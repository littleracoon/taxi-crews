# Cтраница заказа такси с формой и картой. #

небольшое приложение, имитирующее заказ такси.

### Сборка ###

* create react app

### Css ###

* SASS
* Bootstrap 4

### JS ###

* typescript

### Прочее ###

* Redux
* Formik
* Yandex map
* прочие библиотеки
